use std::fmt;
use std::fs::read_to_string;

#[derive(Debug, PartialEq)]
enum Valuechange {
    Increment,
    Decrement,
}

impl fmt::Display for Valuechange {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Valuechange::Increment => write!(f, "Increment"),
            Valuechange::Decrement => write!(f, "Decrement"),
        }
    }
}

#[derive(Debug, PartialEq)]
/// Representation of the eight instructions available in BainFuck
///
/// Each variant represents a command except for Pointer and Byte which take another enum
/// representing either Increment or Decrement.
enum RawInstruction {
    Pointer(Valuechange),
    Byte(Valuechange),
    Output,
    Input,
    WhileOpen,
    WhileClose,
}

impl RawInstruction {
    /// Given a character return an Option containing the corresponding variant of the RawInstruction or None for the rest
    fn from_char(c: char) -> Option<Self> {
        match c {
            '>' => Some(RawInstruction::Pointer(Valuechange::Increment)),
            '<' => Some(RawInstruction::Pointer(Valuechange::Decrement)),
            '+' => Some(RawInstruction::Byte(Valuechange::Increment)),
            '-' => Some(RawInstruction::Byte(Valuechange::Decrement)),
            '.' => Some(RawInstruction::Output),
            ',' => Some(RawInstruction::Input),
            '[' => Some(RawInstruction::WhileOpen),
            ']' => Some(RawInstruction::WhileClose),
            _ => None,
        }
    }
}

impl fmt::Display for RawInstruction {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            RawInstruction::Pointer(change) => write!(f, "Pointer {}", change),
            RawInstruction::Byte(change) => write!(f, "Byte {}", change),
            RawInstruction::Output => write!(f, "Output"),
            RawInstruction::Input => write!(f, "Input"),
            RawInstruction::WhileOpen => write!(f, "Start While"),
            RawInstruction::WhileClose => write!(f, "End While"),
        }
    }
}

#[derive(Debug)]
/// A RawInstruction with it's location in the program added
struct Instruction {
    line: usize,
    column: usize,
    raw: RawInstruction,
}

impl Instruction {
    fn new(line: usize, column: usize, raw: RawInstruction) -> Self {
        Instruction { line, column, raw }
    }
}

impl fmt::Display for Instruction {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "[{}, {}] - {}", self.line, self.column, self.raw)
    }
}

#[derive(Debug)]
/// Representation of a BrainFuck program
///
/// It is composed of the name of the file from which the program and a Vector of the valid
/// instructions parsed from the file.
pub struct BfProgram {
    file_name: String,
    instructions: Vec<Instruction>,
}

impl BfProgram {
    fn new<S: AsRef<str>>(path: S, instructions: Vec<Instruction>) -> Self {
        BfProgram {
            file_name: String::from(path.as_ref()),
            instructions,
        }
    }

    /// Reads the BrainFuck from program from a given file
    ///
    /// This function returns a Result containing the program structure
    pub fn from_file<S: AsRef<str>>(path: S) -> Result<BfProgram, Box<dyn std::error::Error>> {
        let content = read_to_string(path.as_ref())?;

        Ok(BfProgram::new(path, BfProgram::parse_multi_line(&content)))
    }

    fn parse_multi_line(content: &str) -> Vec<Instruction> {
        let mut instructions = Vec::new();

        for (line_count, line) in content.lines().enumerate() {
            for (char_count, c) in line.chars().enumerate() {
                let raw_instruction = RawInstruction::from_char(c);
                if let Some(instruction) = raw_instruction {
                    instructions.push(Instruction::new(line_count, char_count, instruction));
                }
            }
        }

        instructions
    }
}

impl fmt::Display for BfProgram {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let output = self
            .instructions
            .iter()
            .map(|i| format!("{} {}", self.file_name, i))
            .collect::<Vec<_>>()
            .join("\n");

        write!(f, "{}", output)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn parse_pointer_increase_command() {
        let command = RawInstruction::from_char('>');

        assert!(command.is_some());
        assert_eq!(
            RawInstruction::Pointer(Valuechange::Increment),
            command.unwrap()
        );
    }

    #[test]
    fn parse_pointer_decrease_command() {
        let command = RawInstruction::from_char('<');

        assert!(command.is_some());
        assert_eq!(
            RawInstruction::Pointer(Valuechange::Decrement),
            command.unwrap()
        );
    }

    #[test]
    fn parse_byte_increase_command() {
        let command = RawInstruction::from_char('+');

        assert!(command.is_some());
        assert_eq!(
            RawInstruction::Byte(Valuechange::Increment),
            command.unwrap()
        );
    }

    #[test]
    fn parse_byte_decrease_command() {
        let command = RawInstruction::from_char('-');

        assert!(command.is_some());
        assert_eq!(
            RawInstruction::Byte(Valuechange::Decrement),
            command.unwrap()
        );
    }

    #[test]
    fn parse_output_command() {
        let command = RawInstruction::from_char('.');

        assert!(command.is_some());
        assert_eq!(RawInstruction::Output, command.unwrap());
    }

    #[test]
    fn parse_input_command() {
        let command = RawInstruction::from_char(',');

        assert!(command.is_some());
        assert_eq!(RawInstruction::Input, command.unwrap());
    }

    #[test]
    fn parse_open_while_command() {
        let command = RawInstruction::from_char('[');

        assert!(command.is_some());
        assert_eq!(RawInstruction::WhileOpen, command.unwrap());
    }

    #[test]
    fn parse_close_while_command() {
        let command = RawInstruction::from_char(']');

        assert!(command.is_some());
        assert_eq!(RawInstruction::WhileClose, command.unwrap());
    }

    #[test]
    fn parse_single_line() {
        let instructions = BfProgram::parse_multi_line(",,,");

        for (index, instruction) in instructions.iter().enumerate() {
            assert_eq!(0, instruction.line);
            assert_eq!(index, instruction.column);
        }
    }

    #[test]
    fn parse_multiple_lines() {
        let instructions = BfProgram::parse_multi_line(",\n,\n,");

        for (index, instruction) in instructions.iter().enumerate() {
            assert_eq!(index, instruction.line);
            assert_eq!(0, instruction.column);
        }
    }

    #[test]
    fn parse_single_line_with_comments() {
        let mut instructions = BfProgram::parse_multi_line("hello.");

        let instruction = instructions.pop().unwrap();
        assert_eq!(0, instruction.line);
        assert_eq!(5, instruction.column);
    }
}
