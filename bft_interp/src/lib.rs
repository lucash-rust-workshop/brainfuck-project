use bft_types::BfProgram;

#[derive(Debug)]
pub struct VirtualMachine {
    head: u16,
    tape: Vec<u8>,
}

impl VirtualMachine {
    /// Takes the amount of cells this VirtualMachine should have and returns a new VirtualMachine
    /// If the value for the cells is 0 or more than 30000 it will default to 30000
    pub fn new(allocated_cells: usize) -> Self {
        let mut allocated_cells = allocated_cells;
        if allocated_cells == 0 || allocated_cells > 30000 {
            allocated_cells = 30000
        }
        VirtualMachine {
            head: 0,
            tape: Vec::with_capacity(allocated_cells),
        }
    }

    pub fn interprete(&self, program: &BfProgram) {
        println!("{}", program);
    }
}

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
