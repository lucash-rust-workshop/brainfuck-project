use bft_interp::VirtualMachine;
use bft_types::BfProgram;
use std::env::args;

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let file_path = args().nth(1).ok_or("No file path provided")?;

    let program = BfProgram::from_file(file_path)?;
    let vm = VirtualMachine::new(0);

    vm.interprete(&program);
    Ok(())
}
